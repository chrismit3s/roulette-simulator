#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<time.h>
#include<string.h>

#define NUMBER_OF_LOOPS 1000000
#define INSERT          1
#define BUDGET			100

bool get_bool_value(int, int); //x out of y times it's true

void get_roulette_gameplay( long long int,  long long int*,  long long int*);

int main(void) {
	//Counters
	int i;

	//Budgets and Tries
	 long long int   start_budget = (long long int) BUDGET;
	 long long int     max_budget = 0LL,     max_tries = 0LL;
	 long long int average_budget = 0LL, average_tries = 0LL;
	 long long int         budget = 0LL,         tries = 0LL;

	//Time
	clock_t start_t, now_t, end_t, total_t;
	int time_to_finish;

	//Simulate games
	start_t = clock() / CLOCKS_PER_SEC;
	for (i = 0; i < NUMBER_OF_LOOPS; i++) {
		get_roulette_gameplay(start_budget, &budget, &tries);

		//Maximum
		if (max_budget < budget) {
			//more money
			max_budget = budget;
			max_tries  = tries;
		}
		else if (max_budget == budget && max_tries > tries) {
			//Less tries for same amount of money
			max_budget = budget;
			max_tries = tries;
		}

		//Average
		average_budget += budget;
		average_tries  += tries;

		//Clock keeper
		now_t = clock() / CLOCKS_PER_SEC;
		time_to_finish = (int)((NUMBER_OF_LOOPS / (double)(i - 1) - 1) * now_t);

		//Output
		printf("\r%3i%% done. %i:%02i:%02i remaining.      \b\b\b\b\b\b", (int)((double)(i + 1) / NUMBER_OF_LOOPS * 100), time_to_finish / 3600, (time_to_finish / 60) % 60, time_to_finish % 60);
	}
	end_t = clock() / CLOCKS_PER_SEC;
	total_t = end_t - start_t;

	//Calculate Average
	average_budget /= NUMBER_OF_LOOPS;
	average_tries  /= NUMBER_OF_LOOPS;

	//Output
	system("cls");
	printf("TIME:\n");
	printf("%i:%02i:%02i for %i game-simulations (~%.2fms per game)\n\n", (int)total_t / 3600, (int)(total_t / 60) % 60, (int)total_t % 60, NUMBER_OF_LOOPS, (double)total_t / NUMBER_OF_LOOPS * 1000);
	printf("MAX:\n");
	printf("From %lli to %lli in %lli rounds\n\n", start_budget, max_budget, max_tries);
	printf("AVERAGE:\n");
	printf("From %lli to %lli in %lli rounds\n\n", start_budget, average_budget, average_tries);
	return EXIT_SUCCESS;
}

bool get_bool_value(int x, int y) {
	//Set seed
	static bool seed_set = false;
	if (!seed_set) {
		seed_set = true;
		srand((unsigned int)time(0));
	}

	//Get random number
	int r = rand() / (RAND_MAX / y + 1);

	//Larger or smaller
	return r < x ? true : false;
}

void get_roulette_gameplay( long long int budget,  long long int* max_budget,  long long int* tries) {
	long long int insert  = ( long long int)INSERT;
	*max_budget      = budget;
	budget          -= insert;
	*tries           = 0LL;

	while (budget >= 0LL) {
		//Process game
		if (get_bool_value(18, 37)) {
			budget += insert;

			//Reset insert
			insert = ( long long int)INSERT;
		}
		else {
			budget -= insert;

			//Double insert
			insert *= 2;
		}

		//get max budget
		*max_budget = budget > *max_budget ? budget : *max_budget;
		(*tries)++;
	}
}
