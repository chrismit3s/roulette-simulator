#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<time.h>
#include<string.h>

#define NUM_LOOPS 2000000LL

struct game_stats {
	__int64 rounds;
	__int64 high;
};

char * format_time(time_t time, char *str, size_t size);
char * format_num (__int64 num, char *str, size_t size);
char *_format_num (__int64 num, char *str);
bool get_bool_val(int x, int y); //true x out of y times
bool roulette(__int64 start, __int64 stop, __int64 stake, __int64 *rounds, __int64 *max_budget);

int main(void) {
	char time_str [10] = "";
	char  num_str1[19] = "";
	char  num_str2[19] = "";
	char  num_str3[19] = "";

	//parameters
	printf("PARAMETERS\n");
	__int64 start, stop, stake;
	printf("start with: ");
	scanf("%lli", &start);
	printf("stop at:    ");
	scanf("%lli", &stop);
	printf("use stake:  ");
	scanf("%lli", &stake);
	printf("\n");

	//time
	time_t time_start, time_end, time_now, time_remaining;

	//print
	printf("SIMULATION\n");
	printf("simulating %s games from %s to %s\n", format_num(NUM_LOOPS, num_str1, 19), format_num(start, num_str2, 19), format_num(stop, num_str3, 19));

	//zero means dont stop
	stop = (stop == 0) ? LLONG_MAX : stop;

	//simulate NUM_LOOPS games
	__int64 i, games_won = 0LL;
	struct game_stats max = { 0LL, 0LL }, avg = { 0LL, 0LL }, current;
	time_start = time(NULL);
	for (i = 0LL; i != NUM_LOOPS; ++i) {
		//calc average
		if (roulette(start, stop, stake, &(current.rounds), &(current.high)) || stop == LLONG_MAX) {
			avg.rounds += (current.rounds - avg.rounds) / ++games_won;
			avg.high += (current.high - avg.high) / games_won;
		}

		//get max
		if ((max.high < current.high) || (max.high == current.high && max.rounds > current.rounds)) {
			max.high = current.high;
			max.rounds = current.high;
		}

		//get time
		time_now = time(NULL);
		time_remaining = (time_now - time_start) * NUM_LOOPS / (i + 1) - (time_now - time_start);
					/*   (       estimated time  for all games       ) - (     passed time     )   */

		//print "doneness"
		printf("\r%3i%% done - %s remaining",  (int)((i + 1) * 100 / NUM_LOOPS),  format_time(time_remaining, time_str, sizeof(time_str)));
	}
	time_end = time(NULL);
	printf("\n\n\a");

	//output
	printf("TIME\n");
	printf("%s for %s simulations\n", format_time(time_end - time_start, time_str, sizeof(time_str)), format_num(NUM_LOOPS, num_str1, 19));
	printf("~%.2fms per game\n\n", (double)(time_end - time_start) * 1000 / NUM_LOOPS);
	printf("AVERAGE\n");
	printf("from %s to %s in %s rounds\n", format_num(start, num_str1, 19), format_num(avg.high, num_str2, 19), format_num(avg.rounds, num_str3, 19));
	if (stop != LLONG_MAX) printf("finished %i%% of the time\n", (int)(100 * games_won / NUM_LOOPS));
	printf("\n");
	printf("MAX\n");
	printf("from %s to %s in %s rounds\n\n", format_num(start, num_str1, 19), format_num(max.high, num_str2, 19), format_num(max.rounds, num_str3, 19));

	return EXIT_SUCCESS;
}

char *format_time(time_t time, char *str, size_t size) {
	time_t hours, minutes, seconds;

	hours = time / 3600;
	minutes = (time / 60) % 60;
	seconds = time % 60;

	snprintf(str, size, "%02lli:%02lli:%02lli", hours, minutes, seconds);

	return str;
}

char *format_num(__int64 num, char *str, size_t size) {
	size_t characters = 0;
	int padding = 0;

	//for the -
	if (num < 0LL) {
		str[0] = '-';
		num *= -1;
		++padding;
		++characters;
	}

	//count the digits
	__int64 copy = num;
	for (; copy != 0; copy /= 10, ++characters);

	//check if str is large enough (-1 for the null terminator)
	if (characters > size - 1)
		return NULL;

	//already checked if str is large enough, size not needed anymore
	_format_num(num, str + padding);

	return str;
}

char *_format_num(__int64 num, char *str) {
	if (num < 1000LL) {
		sprintf(str, "%d", (int)num);
		return str + ((num < 10) ? 1 : ((num < 100) ? 2 : 3));
	}
	str = _format_num(num / 1000LL, str);
	sprintf(str, "'%03d", (int)(num % 1000LL));
	return str + 4;
}

bool get_bool_val(int x, int y) {
	//Set seed
	static bool seed_set = false;
	if (!seed_set) {
		seed_set = true;
		srand((unsigned int)time(0));
	}

	//Get random number
	int r = rand() / (RAND_MAX / y + 1);

	//Larger or smaller
	return r < x ? true : false;
}

bool roulette(__int64 start, __int64 stop, __int64 stake, __int64 *rounds, __int64 *max_budget) {
	__int64 stake_now = stake;

	//init
	*rounds = 0LL;
	*max_budget = 0LL; 

	//simulate game
	while (start >= stake_now && start < stop) {
		++(*rounds);
		if (get_bool_val(18, 37)) {
			start += stake_now;
			stake_now = stake;
		}
		else {
			start -= stake_now;
			stake_now *= 2;
		}
		
		//get max
		*max_budget = (start > *max_budget) ? start : *max_budget;
	}

	return (start >= stop) ? true : false;
}
